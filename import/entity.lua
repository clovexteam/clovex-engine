-- Imports Entity's from Lua file format
local LuaTable = {}
local Tools = require "engine.tools"
local Entity = require "engine.entity"
local Component = require "engine.component"
local vector = require "engine.vector"

-- Table's, generators
local function entity(name, content, properties, tag)
    local newEntity = {
        name = name or 'entity_'..math.random(1000, 9999),
        nodeName = "entity",
        properties = properties or {},
        source  = type(content) == 'string' and content or nil
    }
    if type(content) == 'table' then
        for k, v in pairs(content) do
            table.insert(newEntity, v)
        end
    end
    return newEntity
end

local function component(name, properties)
    local newComponent =  {
        name = name or error('Component name is not specified'),
        nodeName = "component",
        properties = properties or {}
    }
    return newComponent
end

function LuaTable.import(path)
    if not love.filesystem.exists(path) then 
        error('No such entity "'..path..'"')
    end
    local chunk = assert(loadstring("return " .. love.filesystem.read(path), path))
    setfenv(chunk, 
        {
            entity = entity, 
            component = component, 
            math = math,
            vector = vector
        }
    )
    local result = chunk()
    --print(Tools.tableToString(result))
    return result
end

function LuaTable.load(source)
    local imported = type(source) == 'string' and LuaTable.import(source) or source
    
    local entity = Entity:new()

	if imported.source then
		entity = LuaTable.load(imported.source)
	end

	entity.name = imported.name 

	-- Our parameters from entity will be placed to the transform script
	for name, property in pairs(imported.properties) do
        debug_print(property)
        table.insert(entity.transform.properties, {key = name, value = property})
	end
	
	--table.insert(entity.components, transform)
	for key, childNode in pairs(imported) do
        if type(key) == 'number' and type(childNode) == 'table' then
            local nodeName = childNode.nodeName
            -- to parse children
            if nodeName == 'entity' then
                local loadedEntity = LuaTable.load(childNode)
                table.insert(entity.children, loadedEntity)
                
            elseif nodeName == 'component' then
                local newComponent = Component:new()
                newComponent.name = childNode.name
                newComponent.script = Engine.components.labels[newComponent.name]
                for name, property in pairs(childNode.properties) do
                    table.insert(newComponent.properties, {key = name, value = property})
                end
                table.insert(entity.components, newComponent)
            end
        end
	end
    
	return entity

end

return LuaTable