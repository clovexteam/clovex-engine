local stringTools = require "engine.string.tools"

local ComponentList = {
    paths = {},
	labels = {}
}

-- adds folder where to search for components
function ComponentList:addPath(path)
    table.insert(self.paths, path)
end

-- adds folder where to search for components
function ComponentList:addPaths(paths)
	for k, v in pairs(paths) do
    	self:addPath(v)
	end
end

function ComponentList:find()
	self.labels = {}
	for _, path in pairs(self.paths) do
		self:findRecursive(path)
	end
end

function ComponentList:findRecursive(path)
	local entries = love.filesystem.getDirectoryItems(path)

	for _, dirOrFile in pairs(entries) do
		fullName = path .. '/' .. dirOrFile
		if love.filesystem.isFile(fullName) then
			local splittedName = stringTools.split(dirOrFile, '%.')
			local newName = splittedName[1]
			local fileExtension = splittedName[2]
			if splittedName[2] == 'lua' and self.labels[newName] == nil then
				--print("Component " .. fullName .. ", short '" .. newName .. "'")
				if self.labels[newName] then 
					error(string.format('Component "%d" already exists at "%d"', newName, self.labels[newName]))
				end
				self.labels[newName] = fullName
			end
		elseif love.filesystem.isDirectory(fullName) then
			--print("Directory " .. fullName)
			self:findRecursive(path .. "/" .. dirOrFile)
		end
	end
				
end

function ComponentList:generateComponentName(path)
	local parts = stringTools.split(path, '/')
	parts = stringTools.split(parts[#parts], '.')
	return parts[1]
end

return ComponentList