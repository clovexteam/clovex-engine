--- Collision machine/manager
-- @classmod CollisionMachine 

class = require "engine.class"

local Collision = require "engine.physics.collision"
local CollisionMachine = class("CollisionMachine")

function CollisionMachine:constructor(engine)
    self.engine = Engine
    self.engine.collisions = {}
end
--- Checks the collision between two AABB's
-- @tddo make this in better way
function CollisionMachine:checkCollision(a, b)
    if  a.right > b.left and
        a.left < b.right and
        a.bottom > b.top and
        a.top < b.bottom then
        return true
    else
        return false
    end
end

function CollisionMachine:update()
    table.sort(self.engine.colliders, function (a, b) return a.rect.left < b.rect.left end)
    self.engine.collisions = {}
    --[[
    local colliders = self.engine.colliders
    for i, col1 in ipairs(colliders) do
        col1.collisions = {}
        for j, col2 in ipairs(colliders) do
            if col1 ~= col2 then
                if col1.rect.right < col2.rect.left then goto continue end
                if self:checkCollision(col1.rect, col2.rect) then
                    if col1.collisions[col2.tag] == nil then col1.collisions[col2.tag] = {} end
                            -- Add another to current's collision list
                    table.insert(col1.collisions[col2.tag], col2)
                end
            end
        end
        ::continue::
    end]]
    
    
    local colliders = self.engine.colliders
    for i, current in ipairs(colliders) do
        current.order = i
        current.updateRect()
        for j = i + 1, #colliders do
            local other = colliders[j]
            other.updateRect()
            if current.rect.right < other.rect.left then break end
            -- We will skip all colliders on same Entity
            if current.entity == other.entity then goto continue end 
            if self:checkCollision(current.rect, other.rect) then
                if current.collisions[other.tag] == nil then current.collisions[other.tag] = {} end
                if other.collisions[current.tag] == nil then other.collisions[current.tag] = {} end

                    
                table.insert(other.collisions[current.tag], current)
                table.insert(current.collisions[other.tag], other)

                table.insert(self.engine.collisions, Collision(current, other))
            end
            ::continue::
        end
    end
    
end

function CollisionMachine:lateUpdate()
    for i, v in ipairs(self.engine.collisions) do
        v:applyExitVectors()
    end
    
    for i, collider in ipairs(self.engine.colliders) do
        collider.collisions = {}
    end
end

return CollisionMachine