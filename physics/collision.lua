class = require "engine.class"

Collision = class("Collision")

function Collision:constructor(a, b)
    self.a = a
    self.b = b
end

function Collision:applyExitVectors()
    self.exitVectorB = {x = 0, y = 0}
    
    local x_variant_1 = self.a.rect.right - self.b.rect.left
    local x_variant_2 = self.a.rect.left - self.b.rect.right
    
    if math.abs(x_variant_1) < math.abs(x_variant_2) then
        self.exitVectorB.x = x_variant_1
    else
        self.exitVectorB.x = x_variant_2
    end
    
    local y_variant_1 = self.a.rect.top - self.b.rect.bottom
    local y_variant_2 = self.a.rect.bottom - self.b.rect.top
    
    if math.abs(y_variant_1) < math.abs(y_variant_2) then
        self.exitVectorB.y = y_variant_1
    else
        self.exitVectorB.y = y_variant_2
    end
    
    if math.abs(self.exitVectorB.x) < math.abs(self.exitVectorB.y) then
        self.exitVectorB.y = 0
    else
        self.exitVectorB.x = 0
    end
    
    --debug_print("--------------------------------------------------")
    --debug_print(string.format("Collision between '%s' and '%s', vectors: ", self.a.entity.name, self.b.entity.name))
    
    if self.a.type == "dynamic" and self.b.type == "dynamic" then
        self.b.entity.transform.move(self.exitVectorB.x * .5, self.exitVectorB.y * .5)
        self.a.entity.transform.move(-self.exitVectorB.x * .5, -self.exitVectorB.y * .5)
        
        --debug_print(string.format("A: \t (%d ; %d)", -self.exitVectorB.x * .5, -self.exitVectorB.y * .5))
        --debug_print(string.format("B: \t (%d ; %d)", self.exitVectorB.x * .5, self.exitVectorB.y * .5))
    elseif self.a.type == "static" and self.b.type == "dynamic" then
        self.b.entity.transform.move(self.exitVectorB.x, self.exitVectorB.y)
        --debug_print(string.format("A: \t (%d ; %d)", 0, 0))
        --debug_print(string.format("B: \t (%d ; %d)", self.exitVectorB.x, self.exitVectorB.y))
    elseif self.a.type == "dynamic" and self.b.type == "static" then
        self.a.entity.transform.move(-self.exitVectorB.x, -self.exitVectorB.y)
        --debug_print(string.format("A: \t (%d ; %d)", -self.exitVectorB.x, -self.exitVectorB.y))
        --debug_print(string.format("B: \t (%d ; %d)", 0, 0))
    elseif self.a.type == "static" and self.b.type == "static" then end

    --debug_print("--------------------------------------------------")

--[[
local ax, ay, bx, by = 
        self.a.rect.left + self.a.size.x / 2, 
        self.a.rect.top + self.a.size.y / 2,
        self.b.rect.left + self.b.size.x / 2, 
        self.b.rect.top + self.b.size.y / 2
    
    local exitVector = {x = self.a.rect.right - self.b.rect.left, y = self.a.rect.top - self.b.rect.bottom}
    
    exitVector.x = exitVector.x > 0 and bx + self.b.size.x - ax or bx - self.a.size.x - ax
    exitVector.y = exitVector.y > 0 and by + self.b.size.y - ay or by - self.a.size.y - ay

    if math.abs(exitVector.x) <= math.abs(exitVector.y) then
        exitVector.y = 0
    else
        exitVector.x = 0
    end

    if self.a.type == "dynamic" and self.b.type == "dynamic" then
        debug_print(string.format("For '%s' apply vector (%d ; %d)", self.a.entity.name, exitVector.x, exitVector.y))
        self.b.entity.transform.translate(-exitVector.x * .5, -exitVector.y * .5)
        self.a.entity.transform.translate(exitVector.x * .5, exitVector.y * .5)
    elseif self.a.type == "static" and self.b.type == "dynamic" then
    elseif self.a.type == "dynamic" and self.b.type == "static" then
    elseif self.a.type == "static" and self.b.type == "static" then
    end
]]
end

return Collision