property("animations", {})
property("current", "")
property("currentFrame", 0)
property("frameSize", { x = 0, y = 0} )
property("currentName", "")
property("loop", false)
i = 1

changeTimer = 0

function setAnimation(name)
    currentName = name
    currentFrame = 0
    current = animations[name]
    changeFrame()

end

function changeFrame()
    local columnsCount = sprite.image:getWidth() / frameSize.x
    local x = currentFrame % columnsCount * frameSize.x
    local y = math.floor(currentFrame / columnsCount) * frameSize.y
    print(currentFrame, x, y)
    
    sprite.setBounds(x , y , frameSize.x, frameSize.y)    
end

function load()
    sprite = entity:getComponent("sprite")
    setAnimation(currentName)
    currentFrame = current.frames[i]
    changeFrame()
    
    
end

function update(dt)
    
    changeTimer = changeTimer + dt
    
    if changeTimer > 1 / current.frameRate then 
        changeTimer = 0 
        if i >= #current.frames then
            
            if current.loop then
                i = 1
                currentFrame = current.frames[i]
                
            end    
        else
            i = i + 1
            currentFrame = current.frames[i]
        end
            
        changeFrame()
    end
    
end
