property("path", "")
property("offset", vector(.5, .5))  -- relative to the width and height of the bounding box
property("color", {255, 255, 255, 255})
property("bounds", nil)
property("filter", "nearest", 
    function(v) 
        if image then image:setFilter(v, v) end
        _filter = v
    end,
    function() return _filter end
)

function load()
    position =  entity.transform.position
    rotation =  entity.transform.rotation
    scale =     entity.transform.scale
    
	if path ~= "" then
		image = love.graphics.newImage(path)
	end
    
    if image then
        filter = _filter
        if not bounds then
            setBounds(0, 0, image:getWidth(), image:getHeight())
        end
    end
end

function setBounds(x, y, w, h) 
    bounds = {x = x, y = y, w = w, h = h}
	quad = love.graphics.newQuad(x, y, w, h, image:getDimensions())
end

function onScreen()
    local isOnScreen = 
            position.x + offset.x * bounds.w >= 0 and
            position.y + offset.y * bounds.h >= 0 and
            position.x - offset.x * bounds.w < love.graphics.getWidth() and
            position.y - offset.y * bounds.h < love.graphics.getHeight()

    
    return isOnScreen
end

function draw()

	if onScreen() and image ~= nil then
		local r, g, b, a = love.graphics.getColor()
		love.graphics.setColor(color[1], color[2], color[3], color[4] or 255)
		love.graphics.draw(image, quad, 0, 0, 0, 1, 1, offset.x * bounds.w, offset.y * bounds.h)
		love.graphics.setColor(r, g, b, a)
	end
end