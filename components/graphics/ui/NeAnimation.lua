property("clips", {})            -- ex: {{1, 2, 3}, {4, 3, 2}}
property("frameInterval", 0.5)   -- time between frames
property("loop", false)
property("currentClip", -1)          -- index of the current animation
property("time", 0)
property("currentFrame", 0)

timer = 0

function load()
    sprite = entity:getComponent("sprite")
end

function setAnimation(index, startFrom)
    currentFrame = startFrom or 0
    currentClip = index
end

function update(dt)
    timer += dt

    if timer >= frameInterval then
        timer = 0
        if currentFrame < #clips[currentClip] then
            currentFrame = currentFrame + 1
        elseif loop then 
            currentFrame = 0
        end
    end
end

function updateSpriteBounds(frame)
    local w, h = sprite.image:getWidth() / sprite.w, sprite.image:getHeight() / sprite.h, 
    local x, y = frame % w, frame / w
    sprite.setBounds(x, y, w, h)
end