property("text", "Sample text")
property("color", {255, 255, 255, 255})

function updateFont(fontPath, fontSize)
    if (fontPath == nil) then 
        font = love.graphics.newFont(fontSize)
    else
        font = love.graphics.newFont(fontPath, fontSize)
    end
end

property("fontPath", nil,
    function(value) -- value is a string containing path
        updateFont(value, size)
        _fontPath = value
    end,
    function() -- value is a string containing path
        return _fontPath
    end
)
property("fontSize", 15, 
    function(value) -- value is a string containing path
        updateFont(fontPath, value)
        _fontSize = value
    end,
    function() -- value is a string containing path
        return _fontSize
    end
)
property("charLimit", love.graphics.getWidth())
property("align", "left") -- left, right, center
property("origin", {x = 0, y = 0})
property("scale", {x = 1, y = 1})
property("shearing", {x = 0, y = 0})

function load()
    defaultFont = love.graphics.newFont()
end


function draw()
    local r, g, b, a = love.graphics.getColor()
    if font ~= nil then love.graphics.setFont(font) end
    --print (entity.transform.worldPosition.x .. " " .. entity.transform.worldPosition.y)
    love.graphics.setColor(color[1], color[2], color[3], color[4] or 255)
    love.graphics.print(text, 0, 0)
    love.graphics.printf(text, 0, 0, charLimit, align, 0, scale.x, scale.y, origin.x, origin.y, shearing.x, shearing.y)
    love.graphics.setFont(defaultFont)
    love.graphics.setColor(r, g, b, a)
end

function afterDraw()
end