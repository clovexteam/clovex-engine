property('size', 64) -- grid size of 64 px
property('color', {255, 255, 255, 255}) -- white color

function load()
    transform = entity:getComponent('transform')
end

function draw()
    local r, g, b, a = love.graphics.getColor()
	love.graphics.setColor(color[1] or 255, color[2] or 255, color[3] or 255, color[4] or 255)
    for i=1, 10 do
		love.graphics.line(0, i, love.window.getWidth(), i)
		love.graphics.line(i, 0, i, love.window.getHeight())
    end
	love.graphics.setColor(r, g, b, a)
end