property("center", {x = 0, y = 0}) -- relative
property("size", nil)
property("debugMode", false)
property("type", "dynamic")
property("tag", "")

rect = {}
collisions = {}
order = -1

function load()
    transform = entity.transform
    sprite = entity:getComponent('sprite')
    if not size and sprite then
        size = {x = sprite.bounds.w, y = sprite.bounds.h}
    end
    updateRect()
    -- lets tell about this asshole collider to the world!
    table.insert(engine.colliders, this)
end

function update(dt)
    updateRect()
end

function lateUpdate()

end

function updateRect()
    this.rect.left = transform.position.x + center.x - size.x / 2
    this.rect.top = transform.position.y + center.y - size.y / 2
    this.rect.right = this.rect.left + size.x
    this.rect.bottom = this.rect.top + size.y
end

function collide(tag)
    if collisions[tag] == nil then return nil end
    return collisions[tag][1]
end

function collideAll(tag)
    return collisions[tag]
end

function afterDraw()
    if debugMode then
        updateRect()
        love.graphics.setColor(0, 255, 0)
        love.graphics.rectangle("line", rect.left, rect.top, size.x, size.y) 
        love.graphics.print(order, rect.left + 5, rect.top + 5)
    end
end