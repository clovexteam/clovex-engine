local stringTools = require "engine.string.tools"

local Tools = {}

-- xml may be string (path) or engine.xml.pareser XML tree


-- generates component's name from the path
-- ex: components/transform.lua -> Transform
function Tools.generateComponentName(path)
	local result = ""
	local parts = stringTools.split(path, '/')
	parts = stringTools.split(parts[#parts], '.')
	for i = 1, #parts - 1 do
		result = result .. parts[i]
	end
	return result
end

-- Dumps table content in formatted string
function Tools.tableToString ( t )  
    local result = ""
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            result = result .. "\n" .. indent.."*"..tostring(t)
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        result = result .. "\n" .. indent.."["..pos.."] => "..tostring(t).." {"
                        sub_print_r(val,indent.."  ")
                        result = result .. "\n" .. indent..string.rep(" ",string.len(pos)+3).."}"
                    elseif (type(val)=="string") then
                        result = result .. "\n" .. indent.."["..pos..'] => "'..val..'"'
                    else
                        result = result .. "\n" .. indent.."["..pos.."] => "..tostring(val)
                    end
                end
            else
                result = result .. "\n" .. indent..tostring(t)
            end
        end
    end
    if (type(t)=="table") then
        result = result .. "\n" .. tostring(t).." {"
        sub_print_r(t,"  ")
        result = result .. "\n" .. "}"
    else
        sub_print_r(t,"  ")
    end
    return result
end
return Tools