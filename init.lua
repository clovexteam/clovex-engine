--- Engine
-- Core
-- @classmod Engine

matrix = require "engine.matrix"

local class = require "engine.class"


local Engine = class('Engine')

function Engine:constructor(editorMode)
    self.editorMode = editorMode or false
    self.colliders = {}
    self.components = require "engine.import.components"
    self.components:addPaths({'engine/components', 'assets'})
    self.components:find()

end

function Engine:setEntity(entity)
    -- refresh colliders
    self.colliders = {}
    
    entity:init()
    self.entity = entity
end



return Engine