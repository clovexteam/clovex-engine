--- Component
-- Component - script, that adds some behaviour to the Entity.
-- @classmod Component

-- Class module
local class = require "engine.class"

local Component = class('Component')
local tools = require "engine.tools"
local vector = require "engine.vector"

function Component:constructor() 
    self.properties = {}
    self.scriptProperties = {}
    self._getters = {}
    self._setters = {}
    

    self.loadstring = loadstring
    self.assert = assert
    self.print = debug_print
    self.setfenv = setfenv
    self.pairs = pairs
    self.ipairs = ipairs
    self.error = error
    self.type = type
    self.print_r = print_r
    self.love = love
    self.math = math
    self.camera = camera
    self.matrix = matrix
    self.table = table
    self.tconcat = tconcat
    self.require = require
    self.tonumber = tonumber
    self.tostring = tostring
    self.engine = Engine
    self.this = self
    self.vector = vector
    self.dump = tools.tableToString

    self.editor = {}

    local metaTable = {
        __newindex = function(table, key, value)
            if table == self and type(value) == "function" then
                rawset(table, key, value)
	            setfenv(value, table)
            elseif table._setters[key] then
                table._setters[key](value)
            else
                rawset(table, key, value)
            end
        end,

        __index = function(table, key)
            if (key ~= "_getters" and self._getters[key]) then
                return self._getters[key]()
            else
                return rawget(self, key)
            end
        end
    }
    
    setmetatable(self, metaTable)

    self.init = function(self, name, path, prop)
        if not name then name = self.name end
        if not path then path = self.script end
        if not prop then prop = self.properties end
        self.name = name

        -- all magic is there :)
        self._G = self

        local content = love.filesystem.read(path)
        local worker = assert(loadstring(content, path))
        setfenv(worker, self)

        -- executes component script
        worker()

        -- init all properties
        -- This is done after loading script code to add ability of using
        -- script functions inside set/get
        for k, v in pairs(self.scriptProperties) do
            self.createProperty(v.name, v.default, v.set, v.get);
        end

        if prop ~= nil then
            for _, propertyValue in pairs(prop) do
                self[propertyValue.key] = propertyValue.value
            end
        end
        
        self:load()
        
    end

    self.load = function() end
    self.update = function() end
    self.lateUpdate = function() end
    self.keyPressed = function() end
    self.keyReleased = function() end
    self.draw = function() end
    self.beforeDraw = function() end
    self.afterDraw = function() end
    self.directoryDropped = function() end
    self.fileDropped = function() end
    self.windowFocus = function() end
    self.lowMemory = function() end
    self.mouseFocus = function() end
    self.mouseMoved = function() end
    self.mousePressed = function() end
    self.mouseReleased = function() end
    self.gameQuit = function() end
    self.windowResize = function() end
    self.textEdited = function() end
    self.textInput = function() end
    self.threadError = function() end
    self.touchMoved = function() end
    self.touchPressed = function() end
    self.touchReleased = function() end
    self.windowVisible = function() end
    self.wheelMoved = function() end

    self.editor.draw = function() end
    self.editor.beforeDraw = function() end
    self.editor.afterDraw = function() end

    self.property = function(name, defaultValue, set, get)
        table.insert(self.scriptProperties, {name = name, default = defaultValue, set = set, get = get})
    end
    
    self.createProperty = function(name, defaultValue, set, get)
        if (set == nil and get == nil) then
            _G[name] = defaultValue
            return nil
        end
        _setters[name] = set or function () error('Property "'..name..'" has no setter') end
        _getters[name] = get or function () error('Property "'..name..'" has no getter') end
        
        if (defaultValue ~= nil) then 
            _setters[name](defaultValue) 
        end       
    end
end





return Component