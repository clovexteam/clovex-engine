--- Entity
-- Entity or Object - container for Components and Children.
-- @classmod Entity

-- Class module
local class 	= require "engine.class"
-- Component class
local Component = require "engine.component"

local Entity = class('Entity')

--- List of all Components, attached to this Entity.
Entity.components = {}
--- List of all children, attached to this Entity
Entity.children = {}
--- Parent of this Entity
-- if Entity is root, parent will be null
Entity.parent = nil

--- Tag of this Entity
-- One tag can be applied to amount of Entities
Entity.tag = ''

--- Special editor table, contains events used by CloveX Editor
Entity.editor = {}

--- Creates new Entity but not initialising it
-- Note: all Entities by default have Transform component,
-- and yo don't need in decalration it in entity files by explicit way. 
function Entity:constructor()
	--- List of all Components, attached to this Entity.
	self.components = {}
	--- List of all children, attached to this Entity
	self.children = {}
	--- Parent of this Entity
	-- if Entity is root, parent will be null
	self.parent = nil
    --- Tag of this Entity
    -- One tag can be applied to amount of Entities
    self.tag = ''

	-- All entities have default transform component
	local transform = Component:new()
	transform.script = "engine/components/core/transform.lua"
	transform.name = "transform"

	table.insert(self.components, transform)
	
	-- Quick link
	self.transform = transform
end
--- Initialisation of components and sub-entities (children)
-- @param parent [optional] set this entity as child to "parent" entity
function Entity:init(parent)
	if parent then self.parent = parent end
    
	for _, component in pairs(self.components) do
		component.entity = self
		-- get alive component
		component:init()
	end
	-- do same init things for children (nested entities)
    for _, child in pairs(self.children) do
        child:init(self)
    end
end

--- Attaches new Component to this Entity
-- if path is not specified, quick label will used
-- @param name name of the Component (shortname without .lua)
-- @param path [optional] path to the Component
-- @param prop [optional] custom properties for Component, that will be setted over default
-- @usage local newSpriteComponent = entity:addComponent('sprite', nil, prop = {offset = {x = 10, y = 20}})
-- @return initialised Comonent instance
function Entity:addComponent(name, path, prop) 
	local component = Component:new()
    table.insert(self.components, component)
	if not path and Engine.components.labels[name] then 
		path = Engine.components.labels[name]
	end
	if not path and not Engine.components.labels[name] then
		error('Path to the component isnt specified and it was not found in quick label table')
	end
    component:load(name, path, prop)
	return component
end

--- Attaches list of Components to this Entity
-- @param components table, that contain sequence {name = componentName, path = pathToComponent, prop = customProperties}
-- @see Entity:addComponent
-- @usage entity:addComponents({ name = 'sprite', prop = {offset = {x = 10, y = 20}} }, { name = 'shape' }})
function Entity:addComponents(components) 
	for _, componentValues in pairs(components) do
		self:addComponent(componentValues.name, componentValues.path, componentValues.prop or {})
	end
end

--- Gets component by name
-- @param name name of Component
-- @usage local labelComponent = entity:getComponent('label')
-- @return first component, attached to this Entity with given name
function Entity:getComponent(name) 
	for _, component in pairs(self.components) do
		if component.script == name or component.name == name then
			return component
		end
	end
	return nil
end

--- Gets components by name
-- @param name name of Component(s)
-- @usage local spriteComponents = entity:getComponents('sprite')
-- @return array of all components, attached to this Entity with given name
function Entity:getComponents(name) 
	local result = {}
	for _, component in pairs(self.components) do
		if component.script == name or component.name == name then
			table.insert(result, component)
		end
	end
	if #result == 0 then return nil end
	return result
end

--- Attaches given Entity to this AND initialise it
-- @param entity given Entity to attached
-- @usage local attachedChild = entity:addChild(entityImport.load("/assets/SWEBOK/entities/shot.lua"))
-- @return attached child Entity
-- @see Import.Entity:load
function Entity:addChild(child)
	table.insert(self.children, child)
    child:init(self)
	return child
end

--- Attaches given Entities to this AND initialise it all
-- @param children array of entities to add
-- @see Entity:addChild
function Entity:addChildren(children)
    for _, child in pairs(children) do
		self:addChild(child)
	end
end

--- Finds Entity in list of children with given name
-- @param name Entity's name
-- @param recursive if true, search will be recursive (entity will search inside childs, childs will search in it childs and so on)
-- @usage local neededEntity = entity:findChild('someNmae', true)
function Entity:findChild(name, recursive)
	for _, child in pairs(self.children) do
		if child.name == name then 
			return child
		end
	end
	if recursive then
		for _, child in pairs(self.children) do
			local result =  child:findChild(name, true)
			if result then return result end
		end
	end
	return nil
end

--- Calls given function on every components on this Entity
-- @param name string name of the function
-- @param ... [optional] parameters
function Entity:sendMessage(name, ...)
    for _, component in pairs(self.components) do
		if type(component[name])== "function"then 
			component[name](...)
		end
	end

end

--- Calls given function on every components on this Entity and all of it's Children
-- @param name string name of the function
-- @param ... [optional] parameters
-- @see Entity:sendMessage
function Entity:sendMessageRecursive(name, ...)
    Entity:sendMessage(name, ...)
    for _, component in pairs(self.components) do
		if type(component[name])== "function"then 
			component[name](...)
		end
	end
    for _, child in pairs(self.children) do
        child:sendMessage(name, ...)
    end
end

--- Functions, that can be overriding to add custom behaviour
-- @section Callbacks/Events


--- Callback function used to update the state of the Entity every frame
-- love.update analog
-- @param dt time between frames (delta time)
function Entity:update(dt)
    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.update(dt)
		end
	end
    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.lateUpdate(dt)
		end
	end
	for _, child in pairs(self.children) do
		child:update(dt)
	end	
end

--- Callback function used to execute prerendering things (ex. matrix transformation) of the Entity
function Entity:beforeDraw()
    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.beforeDraw()
		end
	end
end

--- [Editor only] Callback function used to execute prerendering things (ex. matrix transformation) of the Entity 
function Entity.editor:beforeDraw()
    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.editor.beforeDraw()
		end
	end
end

--- Callback function used to process draw loop (beforeDraw, draw, afterDraw) of the Entity
-- love.draw analog
function Entity:draw()
    self:beforeDraw()

    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.draw()
		end
	end
	for _, child in pairs(self.children) do
		child:draw()
	end	

    self:afterDraw()
end

--- [Editor only] Callback function used to process draw loop (beforeDraw, draw, afterDraw) of the Entity
-- love.draw analog
function Entity.editor:draw()
    self.editor:beforeDraw()

    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.editor.draw()
		end
	end
	for _, child in pairs(self.children) do
		child.editor:draw()
	end	

    self.editor:afterDraw()
end

--- Callback function used to execute postrendering things (ex. matrix retransformation) of the Entity
function Entity:afterDraw()
    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.afterDraw()
		end
	end
end

--- [Editor only] Callback function used to execute postrendering things (ex. matrix retransformation) of the Entity
function Entity.editor:afterDraw()
    for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.editor.afterDraw()
		end
	end
end

--- Callback function triggered when a key is pressed
-- @param key pressed keyCode. See Love2d KeyCode
-- love.keypressed analog
function Entity:keyPressed(key) 
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.keyPressed(key)
		end
	end
	for _, child in pairs(self.children) do
		child:keyPressed(key)
	end	
end

--- Callback function triggered when a key is released
-- @param key released keyCode. See Love2d KeyCode
-- love.keyreleased analog
function Entity:keyReleased(key) 
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.keyReleased(key)
		end
	end
	for _, child in pairs(self.children) do
		child:keyReleased(key)
	end	
end

--- Callback function triggered when a directory is dragged and dropped onto the window
-- @param path path of the dropped directory
-- love.directorydropped analog
function Entity:directoryDropped(path)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.directoryDropped(path)
		end
	end
	for _, child in pairs(self.children) do
		child:directoryDropped(path)
	end	
end

--- Callback function triggered when a file is dragged and dropped onto the window
-- @param path path of the dropped file
-- love.filedropped analog
function Entity:fileDropped(path)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.fileDropped(path)
		end
	end
	for _, child in pairs(self.children) do
		child:fileDropped(path)
	end	
end

--- Callback function triggered when window receives or loses focus
-- @param focus true if focused, either false
-- love.focus analog
function Entity:windowFocus(focus)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.windowFocus(focus)
		end
	end
	for _, child in pairs(self.children) do
		child:windowFocus(focus)
	end	
end

--- Callback function triggered when the system is running out of memory on mobile devices
-- love.lowmemory analog
function Entity:lowMemory()
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.lowMemory()
		end
	end
	for _, child in pairs(self.children) do
		child:lowMemory()
	end	
end

--- Callback function triggered when window receives or loses mouse focus
-- @param focus true if focused, either false
-- love.mousefocus analog
function Entity:mouseFocus(focus)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.mouseFocus(focus)
		end
	end
	for _, child in pairs(self.children) do
		child:mouseFocus(focus)
	end	
end

--- Callback function triggered when the mouse is moved
-- @param x X coordinate of the mouse in world space
-- @param y Y coordinate of the mouse in world space
-- @param dx amount moved along the x-axis since the last time mouseMoved was called
-- @param dy amount moved along the y-axis since the last time mouseMoved was called
-- @param istouch true if the mouse button press originated from a touchscreen touch-press
-- love.mousemoved analog
function Entity:mouseMoved(x, y, dx, dy, istouch)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.mouseMoved(x, y, dx, dy, istouch)
		end
	end
	for _, child in pairs(self.children) do
		child:mouseMoved(x, y, dx, dy, istouch)
	end	
end

--- Callback function triggered when the mouse is pressed
-- @param x X coordinate of the mouse in world space
-- @param y Y coordinate of the mouse in world space
-- @param button button index: 1 -  primary, 2 - secondary, 3 - middle and others are mouse-dependent 
-- @param istouch true if the mouse button press originated from a touchscreen touch-press
-- love.mousepressed analog
function Entity:mousePressed(x, y, button, istouch)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.mousePressed(x, y, button, istouch)
		end
	end
	for _, child in pairs(self.children) do
		child:mousePressed(x, y, button, istouch)
	end	
end

--- Callback function triggered when the mouse is pressed
-- @param x X coordinate of the mouse in world space
-- @param y Y coordinate of the mouse in world space
-- @param button button index: 1 -  primary, 2 - secondary, 3 - middle and others are mouse-dependent 
-- @param istouch true if the mouse button press originated from a touchscreen touch-press
-- love.mousereleased analog
function Entity:mouseReleased(x, y, button, istouch)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.mouseReleased(x, y, button, istouch)
		end
	end
	for _, child in pairs(self.children) do
		child:mouseReleased(x, y, button, istouch)
	end	
end

--- Called when the window is resized
-- @param w new width of window
-- @param h new height of window
-- love.resize analog
function Entity:windowResize(w, h)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.windowResize(w, h)
		end
	end
	for _, child in pairs(self.children) do
		child:windowResize(w, h)
	end	
end

--- Called when the candidate text for an IME has changed
-- @param text UTF-8 encoded unicode candidate text
-- @param start start cursor of selected candidate text
-- @param length length of the selected candidate text, may be 0
-- love.textedited analog
function Entity:textEdited(text, start, length)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.textEdited(text, start, length)
		end
	end
	for _, child in pairs(self.children) do
		child:textEdited(text, start, length)
	end	
end

--- Called when text has been entered by the user
-- @param text user's entered text
-- love.textinput analog
function Entity:textInput(text)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
			component.textEdited(text)
		end
	end
	for _, child in pairs(self.children) do
		child:textEdited(text)
	end	
end

--- Callback function triggered when a Thread encounters an error
-- @param thread the thread which produced the error
-- @param errorstr string that contains information about error
-- love.threaderror analog
function Entity:threadError(thread, errorstr)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
			component.threadError(thread, errorstr)
		end
	end
	for _, child in pairs(self.children) do
		child:threadError(thread, errorstr)
	end	
end

--- Callback function triggered when a touch press moves inside the touch screen
-- @param id identifier of the touch
-- @param x X coordinate of the touch in world space
-- @param y Y coordinate of the touch in world space
-- @param dx amount moved along the x-axis since the last time touchMoved was called
-- @param dy amount moved along the y-axis since the last time touchMoved was called
-- @param pressure the amount of pressure being applied, if pressure not supported by hardware, value will be 1
-- love.touchmoved analog
function Entity:touchMoved(id, x, y, dx, dy, pressure)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.touchMoved(id, x, y, dx, dy, pressure)
		end
	end
	for _, child in pairs(self.children) do
		child:touchMoved(id, x, y, dx, dy, pressure)
	end	
end

--- Callback function triggered when the touch screen is touched
-- @param id identifier of the touch
-- @param x X coordinate of the touch in world space
-- @param y Y coordinate of the touch in world space
-- @param dx amount moved along the x-axis since the last time touchPressed was called. This should always be zero
-- @param dy amount moved along the y-axis since the last time touchPressed was called. This should always be zero
-- @param pressure the amount of pressure being applied, if pressure not supported by hardware, value will be 1
-- love.touchpressed analog
function Entity:touchPressed(id, x, y, dx, dy, pressure)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.touchPressed(id, x, y, dx, dy, pressure)
		end
	end
	for _, child in pairs(self.children) do
		child:touchPressed(id, x, y, dx, dy, pressure)
	end	
end

--- Callback function triggered when the touch screen stops being touched
-- @param id identifier of the touch
-- @param x X coordinate of the touch in world space
-- @param y Y coordinate of the touch in world space
-- @param dx amount moved along the x-axis since the last time touchReleased was called. This should always be zero
-- @param dy amount moved along the y-axis since the last time touchReleased was called. This should always be zero
-- @param pressure the amount of pressure being applied, if pressure not supported by hardware, value will be 1
-- love.touchreleased analog
function Entity:touchReleased(id, x, y, dx, dy, pressure)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.touchReleased(id, x, y, dx, dy, pressure)
		end
	end
	for _, child in pairs(self.children) do
		child:touchReleased(id, x, y, dx, dy, pressure)
	end	
end

--- Callback function triggered when window is shown or hidden
-- @param visible true if window is visible, either false
-- love.visible analog
function Entity:windowVisible(visible)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.windowVisible(visible)
		end
	end
	for _, child in pairs(self.children) do
		child:windowVisible(visible)
	end	
end

--- Callback function triggered when the mouse wheel is moved
-- @param x horizontal amount of moving, right is positive
-- @param y vertical amount of moving, up is positive
-- love.wheelmoved analog
function Entity:wheelMoved(x, y)
	for _, component in pairs(self.components) do
		if component.disabled ~= true then
		    component.wheelMoved(x, y)
		end
	end
	for _, child in pairs(self.children) do
		child:wheelMoved(x, y)
	end	
end

return Entity