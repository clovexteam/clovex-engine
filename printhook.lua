local tools = require "engine.tools"
-- print hook
local oldPrint = print
function print(...)
    newArg = {}
    for i,v in pairs(arg) do
        table.insert(newArg, v)
        if type(v) == 'table' then 
            newArg[#newArg] = tools.tableToString(v)
        end
    end
    oldPrint(unpack(newArg))
end