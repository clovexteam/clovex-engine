--- Class
-- @module Class

local ffi

pcall(function()
		 ffi = require 'ffi'
	  end)

local class = {}
local isofclass = {}
local ctypes = {}

-- create a constructor table
local function constructortbl(metatable)
	local ct = {}
	
	setmetatable(ct, {
		__index=metatable,
		__newindex=metatable,
		__metatable=metatable,
		__call=function(self, ...)
			return self.new(...)
		end
	})

   return ct
end

--- Creates new instance of class
-- @param name anme of the class
-- @param parent [optional] parent class
class.new = 
	function(name, parent, ctype)
		local class = {__typename = name}

		if ctype then
			local ctype_id = tonumber(ctype)
			assert(ctype_id, 'invalid ffi ctype')
			assert(not ctypes[ctype_id], string.format('ctype <%s> already considered as <%s>', tostring(ctype), ctypes[ctype_id]))
			ctypes[ctype_id] = name
		end

		class.__index = class

		class.__factory = function()
			local self = {}
			setmetatable(self, class)
			return self
		end

		class.constructor = function() end

		class.new = function(...)
			local self = class.__factory()
			self:constructor(...)
			return self
		end

		isofclass[name] = {[name]=true}

		if parent then
			assert(parent, string.format('parent class <%s> does not exist', parentname))
			setmetatable(class, parent)

			-- consider as type of parent
			while parent do
				isofclass[parent.__typename][name] = true
				parent = getmetatable(parent)
			end

			return constructortbl(class), parent
		else
			return constructortbl(class)
		end
	end


class.factory = 
	function(_class)
		return _class.__factory()
	end

function class.type(obj)
   local tname = type(obj)

   local objname
   if tname == 'cdata' then
	  objname = ctypes[tonumber(ffi.typeof(obj))]
   elseif tname == 'userdata' or tname == 'table' then
	  local mt = getmetatable(obj)
	  if mt then
		 objname = rawget(mt, '__typename')
	  end
   end

   if objname then
	  return objname
   else
	  return tname
   end
end

function class.istype(obj, typename)
   local tname = type(obj)

   local objname
   if tname == 'cdata' then
	  objname = ctypes[tonumber(ffi.typeof(obj))]
   elseif tname == 'userdata' or tname == 'table' then
	  local mt = getmetatable(obj)
	  if mt then
		 objname = rawget(mt, '__typename')
	  end
   end

   if objname then -- we are now sure it is one of our object
	  local valid = rawget(isofclass, typename)
	  if valid then
		 return rawget(valid, objname) or false
	  else
		 return objname == typename -- it might be some other type system
	  end
   else
	  return tname == typename
   end
end

-- allow class() instead of class.new()
setmetatable(class, {__call=
						function(self, ...)
						   return self.new(...)
						end})

return class